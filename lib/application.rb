require 'faraday'
require 'json'

response = Faraday.get("https://servicodados.ibge.gov.br/api/v1/localidades/estados")

data = JSON.parse(response.body, symbolize_names: true)

print "Escolha o ID do Estado desejado: \n"

data.each do |uf|
    print "ID: #{uf[:id]} NOME: #{uf[:nome]} \n"
end

state = gets.to_i

state_response = Faraday.get("https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{state}")

state_data = JSON.parse(state_response.body, symbolize_names: true)


state_data[0][:res].each do |rank|
    print "NOME: #{rank[:nome]}\t| FREQUENCIA: #{rank[:frequencia]}\t| RANKING: #{rank[:ranking]}\n"
end
